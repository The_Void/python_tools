import os
import re
import sys

dirname = input("Enter directory path to search: ")
os.chdir(dirname)
filename = input("What file do you want to scan? ")
lines = []
fullPath = []

for root, dirs, files in os.walk(".", topdown = True):
	if filename in files:
		fullPath.append(os.path.abspath(root))

print("Files Found: ", len(fullPath))

for path in fullPath:
	print(path)

search = input("Search for keywords in file? (y/n): ")

if (search == "y" or search == "Y"):
	keywords = input("Enter keyword: ")
	pattern = re.compile(input("Enter pattern: "), re.M|re.I)

	for path in fullPath:
		os.chdir(path)
		with open(filename, "rt") as read_file:
			for line in read_file:
				line = line.rstrip("\n")
				lines.append(line)

				if keywords in line:
					print("Keyword found. Path to file: ", os.path.abspath(path))
					break
					
				elif pattern.search(line):
					print("Pattern Found. Path to file: ", os.path.abspath(path))
					break

elif (search == "n" or search == "N"):
	searchpattern = input("Search for pattern in file? (y/n): ")

	if (searchpattern == "n" or searchpattern == "N"):
		print("Goodbye")
		sys.exit()

	elif (searchpattern == "y" or searchpattern == "Y" ):
		pattern = re.compile(input("Please enter pattern: "), re.M|re.I)

		for path in fullPath:
			os.chdir(path)
			with open(filename, "rt") as read_file:
				for line in read_file:
					line = line.rstrip("\n")
					lines.append(line)

					if pattern.search(line):
						print("Pattern found. Path to file: ", os.path.abspath(path))
						break
					else:
						print("Pattern not found.")
						break
	else:
		print("Invalid input. Goodbye")
		sys.exit()

else:
	print("Goodbye")

